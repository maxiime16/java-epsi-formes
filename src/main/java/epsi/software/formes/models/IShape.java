package epsi.software.formes.models;

public interface IShape {

    public static final String VERSION = "1.0";
    public void display();
    public Integer getNumber();

}
