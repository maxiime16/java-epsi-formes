package epsi.software.formes.models;

public class Line extends ShapeAbs implements IShape{
    private String symbol = "*" ;

    public Line(int number){
        super(number);
    }
    public Line(){
        this(5);
    }
    @Override
    public void display() {
        for (int i=0;i<getNumber();i++){
            System.out.print(symbol);
        }
    }
}
