package epsi.software.formes;

import java.util.List;
import epsi.software.formes.models.IShape;

public class ShapeServer {

    public static void displayShapeList(List<IShape> formes){
        for(IShape f:formes){
            f.display();
            System.out.println("");
        }
    }
}
